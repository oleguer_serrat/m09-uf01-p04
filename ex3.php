<?php


    function calculaNumeroMesLlarg($matriu){
        // Obtenir numero decimals
        $numDigits = 0;
		for($i=0; $i<count($matriu); $i++){
            for($j=0; $j<count($matriu[$i]); $j++){
                $numDigits_ = strlen((string)$matriu[$i][$j]);
                // echo (string)$matriu[$i][$j]." -> ".strlen((string)$matriu[$i][$j])."\n";
                if ($numDigits_ > $numDigits){
                    $numDigits = $numDigits_;
                }
            }
		}
        return $numDigits;
    }
    
    function calculaCaracters($matriu,$numeroMesLlarg){
        return count($matriu) * $numeroMesLlarg;
    }
    function printCapsalera($numCaracters){
        echo "--".str_repeat(" ", $numCaracters)."--\n";;
    }

    function imprimeixMatrius($matriu){
        $numeroMesLlarg = calculaNumeroMesLlarg($matriu);
        // echo $numeroMesLlarg;
        $numCaracters = calculaCaracters($matriu,$numeroMesLlarg);
        printCapsalera($numCaracters);


		for($i=0; $i<count($matriu); $i++){
            // columna
            echo "|";
            for($j=0; $j<count($matriu[$i]); $j++){
                // fila
                if ($j == count($matriu[$i])-1){
                    echo str_pad($matriu[$i][$j], $numeroMesLlarg, "0", STR_PAD_LEFT)."|";
                }
                else{
                    echo str_pad($matriu[$i][$j], $numeroMesLlarg, "0", STR_PAD_LEFT).",";
                }
            }
            echo "\n";
		}
        printCapsalera($numCaracters);

    }
    function sumarMatrius($matriuA, $matriuB){
        $resultat = [];
    
        $filesA = count($matriuA);
        $columnesA = count($matriuA[0]);
    
        // Si no tenen el mateix nombre d'elemets no es poden sumar
        if (count($matriuB) != $filesA || count($matriuB[0]) != $columnesA) {
            return false;
        }
    
        // Serveix per sumar les matrius
        for ($i = 0; $i < $filesA; $i++) {
            for ($j = 0; $j < $columnesA; $j++) {
                $resultat[$i][$j] = $matriuA[$i][$j] + $matriuB[$i][$j];
            }
        }
    
        return $resultat;
    }
    function multiplicarMatrius($matriuA, $matriuB){
        $resultat = [];
        
        // Si no tenen el mateix nombre d'elemets no es poden multiplicar
        if (count($matriuA[0]) != count($matriuB)) {
            return false;
        }
    
        $filesA = count($matriuA);
        $columnesA = count($matriuA[0]);
        $columnesB = count($matriuB[0]);
    
        // per crear matriu amb tot a zeros per posar el resultat a dins
        for ($i = 0; $i < $filesA; $i++) {
            for ($j = 0; $j < $columnesB; $j++) {
                $resultat[$i][$j] = 0;
            }
        }
    
        // serveix per multiplicar les matrius
        for ($i = 0; $i < $filesA; $i++) {
            for ($j = 0; $j < $columnesB; $j++) {
                for ($k = 0; $k < $columnesA; $k++) {
                    $resultat[$i][$j] += $matriuA[$i][$k] * $matriuB[$k][$j];
                }
            }
        }
    
        return $resultat;
    }
    $matriuA = [[1010101, 1, 5],
                [1, 1, 4],
                [123, 4, 2]];

    $matriuB = [[2, 4, 5],
                [1, 2, 4],
                [1, 44, 123123]];

    echo "\nMATRIU A:\n";
    imprimeixMatrius($matriuA);
    echo "\nMATRIU B:\n";
    imprimeixMatrius($matriuB);
    echo "\nMATRIU A + MATRIU B\n";
    imprimeixMatrius(sumarMatrius($matriuA, $matriuB));
    echo "\nMATRIU A * MATRIU B\n";
    imprimeixMatrius(multiplicarMatrius($matriuA, $matriuB));
    

?>