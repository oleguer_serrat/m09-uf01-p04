<?php
	function separarVocalsDeConsonants($cadenaDeText){
		$vocals = "";
		$altres = "";
		static $VOCALS = ['a', 'e', 'i', 'o', 'u' ];
		for($posicioLletra=0; $posicioLletra<strlen($cadenaDeText); $posicioLletra++){
			if (in_array(strtolower($cadenaDeText[$posicioLletra]), $VOCALS)){
				$vocals = $vocals . $cadenaDeText[$posicioLletra];
			}
			else{
				$altres = $altres . $cadenaDeText[$posicioLletra];
			}
		}
		return $vocals . $altres . "\n";
	}
	echo(separarVocalsDeConsonants("Rafa"));
	echo(separarVocalsDeConsonants("otorrinolaringologo"));
?>